/*
 * IObservable.h
 *
 *  Created on: Jan 24, 2019
 *      Author: marekniemiec
 */

#ifndef IOBSERVABLE_H_
#define IOBSERVABLE_H_

#include "IObserver.h"

#include <list>
#include <memory>

namespace DesignPatterns
{

template<class Value>
class IObservable
{
protected:
	std::list<std::shared_ptr<IObserver<Value>>> observableList; //list is perform in better in case of random erase
	Value observableValue;
public:
	virtual ~IObservable()
	{
	}
	/**
	 * remove observer from list if exist
	 * if not subscribed before nothing will happen
	 */
	virtual void unsubscribe(const std::shared_ptr<IObserver<Value>> &observer)
	{
		observableList.remove(observer);
	}
	/**
	 * push back on the list new observer
	 * no double subscription mechanism
	 */
	virtual void subscribe(const std::shared_ptr<IObserver<Value>> &observer)
	{
		observableList.push_back(observer);
	}
	virtual void update(const Value &val)
	{
		if (val == observableValue)
			return; //nothing to do, they are identical
		observableValue = val;
		for (auto observer : observableList)
		{
			observer->notify(val);
		}
	}
};

} /* namespace DesignPatterns */

#endif /* IOBSERVABLE_H_ */
