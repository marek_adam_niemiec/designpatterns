/*
 * IObserver.h
 *
 *  Created on: Jan 24, 2019
 *      Author: marekniemiec
 */

#ifndef IOBSERVER_H_
#define IOBSERVER_H_

namespace DesignPatterns
{

template<class Value>
class IObserver
{
public:
	virtual ~IObserver()
	{
	}
	/**
	 * override function that will be triggered when subscribed to Observable and value will change
	 */
	virtual void notify(Value v) noexcept = 0;
};

} /* namespace DesignPatterns */

#endif /* IOBSERVER_H_ */
