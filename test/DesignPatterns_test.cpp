//============================================================================
// Name        : DesignPatterns.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
#include "gtest/gtest.h"
#include <iostream>
#include <memory>
using namespace std;

#include "IObserver.h"
#include "IObservable.h"

class observer: public DesignPatterns::IObserver<int>
{
private:
	string name;
	int val;
public:
	observer(const string &n) :
			name(n), val(0)
	{
	}
	void notify(int i) noexcept override
	{
		val = i;
	}
	int getVal() const
	{
		return val;
	}
};
class obserwowany: public DesignPatterns::IObservable<int>
{
public:
	void sprawdzam()
	{
		cout << "wartosc odczytana od obserwowanego " << this->observableValue
				<< endl;
	}
	int size() const
	{
		return observableList.size();
	}
};

TEST(Observer, subscribe)
{
	auto x = std::make_shared<observer>(string("John"));
	auto z = std::make_shared<observer>(string("Andy"));
	obserwowany o;
	ASSERT_NO_THROW(o.subscribe(x));
	ASSERT_NO_THROW(o.subscribe(z));
	ASSERT_NO_THROW(o.update(10));
	ASSERT_NO_THROW(o.sprawdzam());
}

TEST(Observer, unsubscribe)
{
	auto x = std::make_shared<observer>(string("John"));
	obserwowany o;
	ASSERT_NO_THROW(o.subscribe(x));
	ASSERT_NO_THROW(o.update(10));
	ASSERT_EQ(x->getVal(), 10);
	ASSERT_NO_THROW((o.unsubscribe(x)));
	ASSERT_NO_THROW(o.update(20));
	ASSERT_NE(x->getVal(), 20);
}

int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
